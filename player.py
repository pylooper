def isblank(s):
    return s is None or len(s) is 0 or s.isspace()

class Player(object):
    """Provides an interface for the program to produce audio output.
    Implementations should subclass this and do whatever is needed for their
    intended architecture."""

    def __init__(self):
        """button_callback is called with True to set the playpause button to
        active (pushed in with pause showing) and with False to set it to
        inactive (pushed out with play showing)"""

        self.timer = None
        self.status = self.Status()

    def __str__(self):
        """Return a string to use as the label for the current playing song."""
        raise NotImplementedError()

    def play(self):
        raise NotImplementedError()

    def pause(self):
        raise NotImplementedError()

    def stop(self):
        """Stop playing and set the current position to status.begin."""
        raise NotImplementedError()

    def load(self):
        """Load status.filename to prepare for playback."""
        raise NotImplementedError()

    def check_status(self):
        """Check internal status struct and set timer for looper if needed."""
        raise NotImplementedError()

    @property
    def isplaying(self):
        """Are we currently playing?"""
        raise NotImplementedError()

    @property
    def ispaused(self):
        """Are we currently paused?"""
        raise NotImplementedError()

    @property
    def position(self):
        """Get the current position."""
        raise NotImplementedError()

    @property
    def duration(self):
        """Get the length of the current song."""
        raise NotImplementedError()

    class Status(object):
        """Struct class to hold the filename and start and end positions."""
        def __init__(self):
            self.filepath = None
            self.begin = self.end = 0

        def _get_interval(self):
            return self.begin, self.end

        def _set_interval(self, interval):
            self.begin, self.end = interval

        interval = property(_get_interval, _set_interval)

