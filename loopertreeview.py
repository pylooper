from __future__ import with_statement

try:
    import gtk
    import gtk.glade
except:
    raise ImportError('You need gtk and glade')
try:
    import gobject
except:
    raise ImportError('You need gobject')
try:
    from xml.dom.minidom import parseString, parse
except:
    raise ImportError('You need python-xml')

import os.path

class XMLLoop(object):
    """An abstraction layer for all the XML crap necessary for these loops."""

    def __init__(self, file=None):
        if file and os.path.exists(file):
            self.doc = parse(file)
        else:
            self.doc = parseString(u'<!DOCTYPE loopobj SYSTEM "loopobj.dtd"><loopobj />'.encode('UTF-8'))

        self.toxml = self.doc.toxml
        self.toprettyxml = self.doc.toprettyxml

    def _get_filepath(self):
        return getCBTNText(self.doc, u'filepath')

    def _set_filepath(self, filepath):
        if len(self.doc.getElementsByTagName(u'filepath')) > 0:
            getChildByTagName(self.doc, u'filepath').firstChild.data = filepath
        else:
            filepath_node = self.doc.createElement(u'filepath')
            filepath_node.appendChild(self.doc.createTextNode(filepath))
            self.doc.documentElement.appendChild(filepath_node)

    filepath = property(_get_filepath, _set_filepath)
    
    def get_intervals(self):
        """Returns a list of intervals, where each interval is a list of four elements:
         - name
         - start point
         - end point
         - whether this is the selected interval or not
         
        For example, get_intervals might return the following:
            [u'loop 1', 0, 150, False]"""

        return [[getCBTNText(interval, tag)
                    for tag in [u'name', u'start', u'end']
                    ]
                        for interval in self.doc.getElementsByTagName(u'interval')]

    def add_interval(self, interval):
        """Adds the requested interval, which must be of the same form get_intervals returns"""

        interval_node = self.doc.createElement(u'interval')

        name = self.doc.createElement(u'name')
        name.appendChild(self.doc.createTextNode(interval[0]))
        interval_node.appendChild(name)
        
        start = self.doc.createElement(u'start')
        start.appendChild(self.doc.createTextNode(str(interval[1])))
        interval_node.appendChild(start)

        end = self.doc.createElement(u'end')
        end.appendChild(self.doc.createTextNode(str(interval[2])))
        interval_node.appendChild(end)

        loopobj = self.doc.documentElement
        loopobj.appendChild(interval_node)

# XXX These are convenience methods that do not make sure you aren't stupid.
#     Perform this check yourself before using them.
def getChildByTagName(node, tag):
    return node.getElementsByTagName(tag)[0]
def getText(node):
    return node.firstChild.data
def getCBTNText(node, tag):
    return getText(getChildByTagName(node, tag))

class LooperTreeView(object):
    def __init__(self, wTree, pylooper=None):
        self.pylooper = pylooper

        self.treeview = wTree.get_widget('treeview')
        self.liststore = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_INT64, gobject.TYPE_INT64)
        self.treeview.set_model(self.liststore)

        cell0 = gtk.CellRendererText()
        cell1 = gtk.CellRendererText()
        cell2 = gtk.CellRendererText()
        self.treeview.append_column(gtk.TreeViewColumn('Name', cell0, text=0, editable=True))
        self.treeview.append_column(gtk.TreeViewColumn('Begin', cell1))
        self.treeview.append_column(gtk.TreeViewColumn('End', cell2))

        cell0.connect('edited', self.name_edited)

        self.treeview.get_column(1).set_cell_data_func(cell1, nsecs2str, 1)
        self.treeview.get_column(2).set_cell_data_func(cell2, nsecs2str, 2)

        self.treeview.get_selection().set_mode(gtk.SELECTION_SINGLE)
        self.treeview.get_selection().connect('changed', self.changed)

    def add(self, begin, end, name='new_interval'):
        iter = self.liststore.append([name, long(begin), long(end)])
        if iter:
            self.treeview.get_selection().select_iter(iter)

    def changed(self, treeselection):
        model, iter = treeselection.get_selected()
        if iter:
            self.pylooper.update_interval(model.get_value(iter, 1), model.get_value(iter, 2))
            return True
        else:
            return False

    def name_edited(self, cellrenderertext, path, new_text):
        try:
            self.liststore.set(self.liststore.get_iter_from_string(path), 0, new_text)
            return True
        except:
            return False

    def update_interval(self, begin, end):
        if self.treeview.get_selection().get_selected()[1]:
            self.liststore.set(self.treeview.get_selection().get_selected()[1], 1, begin, 2, end)
            return True
        else:
            return False

    def clear(self):
        self.liststore.clear()

    def save(self, filepath):
        xlp = XMLLoop()
        xlp.filepath = self.pylooper.player.status.filepath
        
        def callback_add(model, path, iter):
            xlp.add_interval([model.get_value(iter, 0), model.get_value(iter, 1), model.get_value(iter, 2)])
            return False

        self.liststore.foreach(callback_add)

        with open(filepath, 'w') as file:
            file.write(xlp.toxml())
        return True

    def open(self, filepath):
        try:
            xlp = XMLLoop(filepath)
        except:
            return False

        oldpath = self.pylooper.player.status.filepath
        self.pylooper.player.status.filepath = xlp.filepath

        # We need to do this, or when we call self.add, the changed event will
        # fail with duration = 0
        if not self.pylooper.player.load():
            self.pylooper.player.status.filepath = oldpath
            return False

        self.clear()

        for interval in xlp.get_intervals():
            self.add(interval[1], interval[2], interval[0])

    def delete(self, widget):
        treeselection = self.treeview.get_selection()
        model, iter = treeselection.get_selected()
        if iter and model.remove(iter):
            treeselection.select_iter(iter)

def nsecs2str(treeviewcolumn, cellrenderer, model, iter, column):
    nsecs = model.get_value(iter, column)
    cellrenderer.set_property('text', "%(m)02d:%(s)02d.%(ms)03d" % {
        'm': nsecs / 60000000000,
        's': (nsecs / 1000000000) % 60,
        'ms': (nsecs / 1000000) % 1000})

