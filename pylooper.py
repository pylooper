#!/usr/bin/python

try:
    import gtk
    import gtk.glade
except:
    raise ImportError('You need gtk and glade')
try:
    import gobject
except:
    raise ImportError('You need gobject')
try:
    import gnome.ui
except:
    raise ImportError('You need python-gnome')

from looperwidget import LooperWidget
from loopertreeview import LooperTreeView
from gstplayer import GstPlayer
import settings

class PyLooper(object):
    """The main class that creates windows and starts things going."""

    def __init__(self, gladefile=settings.GLADEFILE):
        gnome.init('PyLooper', '0.1.0')

        self.gladefile = gladefile
        self.wTree = gtk.glade.XML(self.gladefile)

        self.player = GstPlayer()

        self.window = self.wTree.get_widget('app1')
        self.looperwidget = LooperWidget(update_callback=self.update_interval,
                player=self.player)
        self.wTree.get_widget('vbox1').pack_start(self.looperwidget, expand=False)

        self.aboutdialog = AboutDialog(self.wTree)
        self.savefilechooserdialog = SaveFileChooserDialog(self.wTree)
        self.loopertreeview = LooperTreeView(self.wTree, self)
        self.playpausebutton = self.wTree.get_widget('playpausebutton')
        self.openfilechooserbutton = self.wTree.get_widget('openfilechooserbutton')

        self.connect_all()
        self.window.show_all()

    def connect_all(self):
        connections = {
                'gtk_main_quit': self.exit,
                'show_about_dialog': self.show_about_dialog,
                'gtk_widget_destroy': gtk.Widget.destroy,
                'open': self.open,
                'playpause': self.playpause,
                'add': self.add,
                'save': self.save,
                'delete': self.loopertreeview.delete
                }
        self.wTree.signal_autoconnect(connections)

    def show_about_dialog(self, widget):
        self.aboutdialog.show()
        return True

    def open(self, widget):
        filepath = self.openfilechooserbutton.get_filename()
        # this ain't great, but it's alright
        if filepath and filepath.endswith('.xml'):
            self.loopertreeview.open(filepath)
        else:
            self.player.status.filepath = filepath
       	    if self.player.status.filepath and self.player.load():
       	        self.playpausebutton.set_label('gtk-media-play')
       	        self.loopertreeview.clear()
       	        self.looperwidget.set_interval(0, self.player.duration, self.player.duration)
        return True

    def save(self, widget):
        response, path = self.savefilechooserdialog.show()
        if response == gtk.RESPONSE_OK:
            self.loopertreeview.save(path)
        return True

    def playpause(self, widget):
        if widget.get_label() == 'gtk-media-play':
            if self.player.play():
                widget.set_label('gtk-media-pause')
        else:
            if self.player.pause():
                widget.set_label('gtk-media-play')
        return True

    def add(self, widget):
        self.loopertreeview.add(*self.player.status.interval)

    def update_interval(self, begin=None, end=None):
        if begin is not None and end is not None:
            # we were called by loopertreeview, so we should update looperwidget
            self.looperwidget.set_interval(begin, end, self.player.duration)
        else:
            # we were called by looperwidget, so we should update the treeview
            self.loopertreeview.update_interval(*self.looperwidget.interval(self.player.duration))

        self.player.status.interval = self.looperwidget.interval(self.player.duration)
        self.player.check_status()

    def exit(self, widget):
        gtk.main_quit()

class AboutDialog(object):
    def __init__(self, wTree):
        self.dlg = wTree.get_widget('aboutdialog1')

    def show(self):
        self.dlg.run()
        self.dlg.hide()

class SaveFileChooserDialog(object):
    def __init__(self, wTree):
        self.dlg = wTree.get_widget('savefilechooserdialog')
        filter = gtk.FileFilter()
        filter.set_name('Saved Loops (XML)')
        filter.add_pattern('*.xml')
        self.dlg.add_filter(filter)

    def show(self):
        response = self.dlg.run()
        path = self.dlg.get_filename()
        self.dlg.hide()
        return response, path

if __name__ == '__main__':
    window = PyLooper()
    gtk.gdk.threads_init()
    gtk.main()
