try:
    import gtk
    from gtk import gdk
except:
    raise ImportError('You need gtk')
try:
    import gobject
except:
    raise ImportError('You need gobject')

import settings

class LooperWidget(gtk.DrawingArea):
    """Creates and manages the interface by which start and end looping points
    are chosen and manipulated.
    
    This is a pretty portable, generic widget that can be used for any
    application requiring an interval input of some sort.  Maybe I should
    rename it IntervalWidget."""

    def __init__(self, update_callback, player=None, width=100, height=32):
        """Registers update_callback and player for future use, and sets up gtk
        stuff for the widget.

         - update_callback() is called whenever the endpoints change
         - player is used for its methods 'position' and 'duration' to display the
           progress bar"""

        super(LooperWidget, self).__init__()

        self.connect('expose-event', LooperWidget.expose_event)
        self.connect('configure-event', LooperWidget.expose_event)
        self.connect('motion-notify-event', LooperWidget.motion_notify_event)
        self.connect('button-release-event', LooperWidget.button_release_event)

        self.add_events(gdk.EXPOSURE_MASK
                | gdk.POINTER_MOTION_MASK
                | gdk.BUTTON1_MOTION_MASK
                | gdk.BUTTON3_MOTION_MASK
                | gdk.BUTTON_PRESS_MASK
                | gdk.BUTTON_RELEASE_MASK)

        self.set_size_request(width, height)

        self.update_callback = update_callback
        self.player = player

        self.raw_begin = 0.
        self.raw_end = 1.

        # expose events ain't too slow, so we can afford this (I think)
        gobject.timeout_add(settings.GRAPHICSTIMEOUTLENGTH, self.interval_queue_draw)

    def interval(self, length=None):
        """Returns the interval selected, scaled by length and then quantized,
        if length is provided."""
        if not length:
            return self.raw_begin, self.raw_end
        else:
            return long(length * self.raw_begin), long(length * self.raw_end)

    def set_interval(self, begin, end, duration):
        if begin is None or end is None:
            return False

        self.update_begin(begin, duration)
        self.update_end(end, duration)

    @property
    def width(self):
        """Shorthand for getting the widget's width."""
        return self.get_allocation().width

    @property
    def height(self):
        """Shorthand for getting the widget's height."""
        return self.get_allocation().height

    @property
    def size(self):
        """Shorthand for getting the widget's size (width, height)."""
        return self.width, self.height

    def update_begin(self, begin, duration):
        self.raw_begin = begin / duration
        if self.raw_begin < 0:
            self.raw_begin = 0
        if self.raw_begin > self.raw_end:
            self.raw_end = self.raw_begin

        self.queue_draw()

    def update_end(self, end, duration):
        self.raw_end = end / duration
        if self.raw_end > 1:
            self.raw_end = 1
        if self.raw_begin > self.raw_end:
            self.raw_begin = self.raw_end

        self.queue_draw()

    def update_begin_raw(self, x):
        """Updates the begin endpoint and calls requisite callbacks."""
        self.raw_begin = x / self.width
        if self.raw_begin < 0:
            self.raw_begin = 0
        if self.raw_begin > self.raw_end:
            self.raw_end = self.raw_begin

        self.queue_draw()

    def update_end_raw(self, x):
        """Updates the end endpoint and calls requisite callbacks."""
        self.raw_end = x / self.width
        if self.raw_end > 1:
            self.raw_end = 1
        if self.raw_begin > self.raw_end:
            self.raw_begin = self.raw_end

        self.queue_draw()

    def reset_endpoints(self, raw_begin=0, raw_end=1):
        """Resets endpoints and calls requisite callbacks."""
        self.raw_begin = raw_begin
        self.raw_end = raw_end

        self.update_callback()
        self.queue_draw()

    def interval_queue_draw(self):
        self.queue_draw()
        # so gobject will keep calling us
        return True

    def expose_event(self, event):
        """Event handler for expose events.  Just draws everything over again,
        pretty simple really.  You can change the color scheme by providing a
        different graphics context to each command."""

        self.window.draw_rectangle(self.style.bg_gc[settings.WIDGET_STATE], True, 0, 0, *self.size)

        self.window.draw_rectangle(self.style.light_gc[settings.WIDGET_STATE], True,
                int(self.width * self.raw_begin), 0,
                int(self.width * (self.raw_end - self.raw_begin)),
                self.height - 1)

        self.window.draw_rectangle(self.style.dark_gc[settings.WIDGET_STATE], False,
                int(self.width * self.raw_begin), 0,
                int(self.width * (self.raw_end - self.raw_begin)),
                self.height - 1)

        # If this is being used for something other than PyLooper, we may not
        # have a player to query, so checking for that first is more portable.
        if self.player and self.player.duration > 0:
            position = int(1. * self.width * self.player.position / self.player.duration)
            self.window.draw_line(self.style.mid_gc[settings.WIDGET_STATE], position, 1, position, self.height - 2)

        return True

    def motion_notify_event(self, event):
        """Event handler for motion events.  Gets the x coordinate and updates
        the correct endpoint based on which button is held down."""

        # TODO: If you are a gtk wizard, maybe you can condense these four
        # lines down to something less dumb looking (one obvious way and all
        # that).  I just stole this from a tutorial somewhere.
        if event.is_hint:
            x, y, state = event.window.get_pointer()
        else:
            x, y, state = event.x, event.y, event.state

        if (state & gdk.BUTTON1_MASK) and not (state & gdk.BUTTON3_MASK):
            self.update_begin_raw(x)
            self.update_callback()
        elif (state & gdk.BUTTON3_MASK) and not (state & gdk.BUTTON1_MASK):
            self.update_end_raw(x)
            self.update_callback()

        return True

    def button_release_event(self, event):
        """Event handler for button release events.  Basically the same as
        motion_notify_event, but the event apis seem very non-uniform, so this
        is kind of weird.  It works for now though."""

        if len(event.get_coords()) < 2:
            return False

        if event.button is 1:
            self.update_begin_raw(event.get_coords()[0])
            self.update_callback()
        elif event.button is 3:
            self.update_end_raw(event.get_coords()[0])
            self.update_callback()

        return True

