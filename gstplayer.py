import os, os.path
from mutagen.mp3 import MP3

try:
    import pygst
    pygst.require('0.10')
    import gst
except:
    raise ImportError('You need gstreamer version >=0.10 and its python bindings')
try:
    import gobject
except:
    raise ImportError('You need gobject')

from player import Player, isblank
import settings

class GstPlayer(Player):
    """Implements Player with the python gstreamer bindings."""

    def __init__(self):
        super(GstPlayer, self).__init__()

        self.player = gst.element_factory_make('playbin', 'player')
        self.audio = None

    @property
    def isplaying(self):
        if self.player and self.player.get_state()[1] is gst.STATE_PLAYING:
            return True
        else:
            return False

    @property
    def ispaused(self):
        if self.player and self.player.get_state()[1] is not gst.STATE_PAUSED:
            return False
        else:
            return True

    @property
    def position(self):
        try:
            return self.player.query_position(gst.Format(gst.FORMAT_TIME), None)[0]
        except:
            return 0

    @property
    def duration(self):
        if self.audio:
            # we do everything in nanoseconds like gstreamer does
            return self.audio.info.length * 1000000000
        else:
            return 0

    def play(self):
        if isblank(self.status.filepath):
            return False

        if isblank(self.player.get_property('uri')):
            self.load()

        if not gst.uri_is_valid(self.player.get_property('uri')):
            return false

        self.seek_if_needed()
        if self.player.set_state(gst.STATE_PLAYING) is gst.STATE_CHANGE_FAILURE:
            return False
        self.timer = self.new_timer()
        return True

    def pause(self):
        if self.player.set_state(gst.STATE_PAUSED) is gst.STATE_CHANGE_FAILURE:
            return False
        gobject.source_remove(self.timer)
        return True

    def stop(self, widget=None):
        self.player.seek_simple(gst.Format(gst.FORMAT_TIME), gst.SEEK_FLAG_FLUSH, self.status.begin)

        if self.player.set_state(gst.STATE_NULL) is gst.STATE_CHANGE_FAILURE:
            return False

        if self.timer:
            gobject.source_remove(self.timer)

        return True

    def load(self):
        self.status.begin = 0
        if not self.stop():
            return False

        if os.path.exists(self.status.filepath):
            self.player.set_property('uri', 'file://' + self.status.filepath)
            self.player.set_state(gst.STATE_READY)

            self.audio = MP3(self.status.filepath)
            self.status.end = self.duration
            return True

        return False

    def check_status(self):
        if self.timer:
            gobject.source_remove(self.timer)

        wasplaying = self.isplaying

        self.seek_if_needed()

        if wasplaying:
            self.timer = self.new_timer()

    def seek_if_needed(self):
        if not self.status.begin <= self.position < self.status.end:
            self.player.seek_simple(gst.Format(gst.FORMAT_TIME),
                    gst.SEEK_FLAG_FLUSH, long(self.status.begin))

    def new_timer(self):
        return gobject.timeout_add(int(min(self.status.end - self.position, 1000000000) / 1000000), self.check_status)

