try:
    import gtk
except:
    raise ImportError('You need gtk')

# which gladefile to use
GLADEFILE = 'pylooper.glade'

# how long to sleep between updating the progress bar, in millisecs
GRAPHICSTIMEOUTLENGTH = 250

# determines which set of colors from the scheme we pull to make our widget
WIDGET_STATE = gtk.STATE_SELECTED
